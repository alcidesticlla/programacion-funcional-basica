fun alternativa_s(list : int list, factor:int) =
  if null list
  then
      0
  else
      
      if (factor = 1 )
      then
	  hd list * factor  + alternativa_s(tl list, ~1)
      else
	  hd list * factor + alternativa_s(tl list, 1)
  

  (*[4,5,3,2] --> 4 -5 +3 -2  = 0
    [4,5,3,2]  ---> 1
    [5,3,2] ---> -1
    [3,2] ---> 1
    [2]---> -1  
    [0]-->1  


    [1,2,3,4,5] --> 1
    [2,3,4,5] --> -1
    [3,4,5] ---> 1
    [4,5]----> -1
    [5] ---> 1
    []---> -1
    *)
fun alternativa (list: int list)=
  if (null list)
  then 0
  else
      alternativa_s(list,1)

		   

(*hola ...!! OPTIONAL*)	 
fun hola(nombre: string) =
  if (size (nombre) = 0)
  then
      "hola...tu!!"
  else
      "hola... "^nombre^"!!"
	  

(* 
- hola1(SOME "alcides");
val it = "hola..alcides..!!!" : string
- hola1(NONE);
val it = "hola...!!!" : string
- 
*)
fun hola1(nombre: string option)=
  if(isSome nombre)
  then
      "hola "^ (valOf nombre) ^"..!!!"
  else
      "hola...!!!"
