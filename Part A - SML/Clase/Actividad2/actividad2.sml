(*Autor: Alcides Ticlla Choque *)

(* Ejercicio 1: is older 
val test1 = is_older ((1,2,3),(2,3,4)) = true*)
fun is_older (date1: int * int * int , date2: int * int * int)=
  let    
   val day1 = ((#1 date1) * 365) + ((#2 date1) * 30) + (#3 date1); 
   val day2 = ((#1 date2) * 365) + ((#2 date2) * 30) + (#3 date2);
  in
      day1 < day2
  end



      
(* Ejercicio 2: number in month
val test2 = number_in_month ([(2012,2,28),(2013,12,1)],2) = 1*)
fun number_in_month (dates: (int * int * int) list, month:int)=
  if null dates
  then 0
  else
      if (#2 (hd dates)) = month
      then 1 + number_in_month(tl dates, month)
      else 0 + number_in_month(tl dates, month)


			      

(* Ejercicio 3: number in months 
val test3 = number_in_months ([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4]) = 3*)
fun number_in_months (dates: (int * int * int) list, months: int list) =
  if (null months)
  then 0
  else
      number_in_month(dates,hd months) + number_in_months(dates,tl months)


							 
							 
(* Ejercio 4: dates in month
val test4 = dates_in_month ([(2012,2,28),(2013,12,1)],2) = [(2012,2,28)]*)
fun dates_in_month (dates: (int * int * int )list, month:int) =
  if (null dates)
  then []
  else
      if(#2 (hd dates) = month)
      then
	  (hd dates)::dates_in_month(tl dates, month)
      else
	  dates_in_month(tl dates, month)


			
(* Ejercicio 5: dates in month
val test5 = dates_in_months ([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4]) = [(2012,2,28),(2011,3,31),(2011,4,28)]
*)
fun dates_in_months (dates: (int * int * int )list, months:int list) =
  if (null months)
  then []
  else
      (hd(dates_in_month(dates,hd months)))::dates_in_months(dates,tl months)


(*Ejercicio 6: get_nth
val test6 = get_nth (["hi", "there", "how", "are", "you"], 2) = "there"
Retorna NONE si el index no esta en la lista de cade 
*)


fun get_nthhelper(cadenas: string list, n : int , index : int)=

  if(n = index)
  then
      hd cadenas
  else
      get_nthhelper(tl cadenas, n, index + 1);
	
							    

fun get_nth (cadenas: string list, n:int) =
  if(null cadenas orelse (length cadenas)  < n)
  then
      "NONE"
  else
      
      get_nthhelper(cadenas,n,1);


(*Ejercicio 7: date_to_string
date_to_string (2013, 6, 1) = "June 1, 2013"
*)
fun date_to_string (date: int * int * int) =
  let
      val months = ["January","February","March","April","May","June","July","August","September","Octubre","November","Dicember"]
      val month = get_nth(months,#2 date)
      val year =  Int.toString (#1 date)
	val day =  Int.toString  (#3 date)		      
      
  in
      month^" "^day^", "^year
  end
      




(*Ejercicio 8: number_before_reaching_sum
number_before_reaching_sum (10, [1,2,3,4,5]) = 3*)

fun number_before_reaching_sum(n: int, numbers: int list)=
  1


      
(*Ejercicio 9 wht month
what_month 70 = 3*)
fun what_month (month: int) =
  if (month div 30)= 0
  then
      month div 30
  else
      (month div 30) + 1


(*Ejercicio 10 month_range
month_range (31, 34) = [1,2,2,2]*)


(*Ejercicio 11 oldest
oldest([(2012,2,28),(2011,3,31),(2011,4,28)]) = SOME (2011,3,31)*)

fun to_days(date:(int * int * int))=
  let
      val day = ((#1 date) * 365) + ((#2 date) * 30) + (#3 date);
  in
      day
  end
      
