import pdb

class pila():
	def __init__(self,size):
		self.size = size
		self.elementos = []

	def pilaVacia(self):
		return len(self.elementos)  == 0

	def pilaLlena(self):
		return len(self.elementos) == self.size	

	def apilar(self, elemento):
		if (not self.pilaLlena()):
			self.elementos.append(elemento)
		else:
			print("La pilla esta llena")	

	def desapilar(self):
		a = 0
		if (not self.pilaVacia()):
			a = self.elementos[len(self.elementos)-1]
			self.elementos = self.elementos[:-1]
		else:
			print("La pila esta vacia")

		return a
	def leerPila (self):
		for a in range(self.size):
			elem = input("Intoducir lemento: ")
			self.apilar(int(elem))

	def imprimirPila(self):
		print(self.elementos)

	def cima(self):
		a = 0
		if (not self.pilaVacia()):
			a = self.elementos[len(self.elementos)-1]
		else:
			print("La pila esta vacia")

		return a

	def numElemPila(self):
		return len(self.elementos)
	

	def decapitar(self):
		if (not self.pilaVacia()):
			self.elementos = self.elementos[:-1]
		else:
			print("La pila esta vacia")

	def eliminarPila(self):
		self.elementos = []

	def TamPila(self):
		return self.size

#Ejercicios

#Imprimir el contenido de una pila de enteros sin cambiar su contenido.
	def imprimirContenido(self):
		print(self.elementos)


# Colocar en el fondo de una pila un nuevo elemento.

	def colocarFondo(self,elemento):
		auxPilaA = pila(self.size)
		while(not self.pilaVacia()):
			auxPilaA.apilar(self.desapilar())

		self.apilar(elemento)		
		
		while(not auxPilaA.pilaVacia()):
			self.apilar(auxPilaA.desapilar())	


#Calcular el numero de elementos de una pila sin modificar su contenido.
	def calcularTam(self):
		self.TamPila()
		
# Eliminar de una pila todas las ocurrencias de un elemento dado.
	def eliminarOcurrencia(self,elemento):
		pilaAux = pila(self.size)
		while(not self.pilaVacia()):
			pilaAux.apilar(self.desapilar())
		
		while(not pilaAux.pilaVacia()):
			aux = pilaAux.desapilar()
			if(aux != elemento):
				self.apilar(aux)
# Intercambiar los valores del tope y el fondo de una pila.

# Duplicar el contenido de una pila.

	def duplicar(self):
		auxPila1 = pila(self.size)
		auxPila2 = pila(self.size)

		while (self.pilaVacia() == False):
			elem = self.desapilar()
			auxPila1.apilar(elem)

		while( auxPila1.pilaVacia() == False):
			aux = auxPila1.desapilar()
			auxPila2.apilar(aux)
			self.apilar(aux)

		return auxPila2	

# Verificar si el contenido de una pila de caracteres es un palindrome.
	def palindrome(self, pila):
		if (self.TamPila() != pila.TamPila()):
			return False
		return True # falta implementar	
			
# Calcular la suma de una pila de enteros sin modificar su contenido.
	def calcularSuma(self):
		pilaAux = self.duplicar()
		sum = 0
		while(not pilaAux.pilaVacia()):
			sum += pilaAux.desapilar()
		return sum

# Comprobar si un elemento pertenece a una pila.

	def buscar(self, elemento):
		pilaAux = self.duplicar()
		while (not pilaAux.pilaVacia()):
			if (elemento == pilaAux.desapilar()):
				return True
		return False
		
# Realizar la interseccion de dos pilas.

	def Interseccion(self, pilaB):	
		pilaAux = self.duplicar()
		pileResult = pila(self.size)
		#pdb.set_trace()
		while(not pilaB.pilaVacia()):
			aux = pilaB.desapilar()
			if (pilaAux.buscar(aux)):
				pileResult.apilar(aux)
		
		return pileResult
		
# Realizar la union de dos pilas.
	
a = pila(6)	
a.apilar(1)
a.apilar(2)
a.apilar(3)
a.apilar(1)
a.apilar(4)
a.apilar(1)
a.imprimirContenido()

a.eliminarOcurrencia(1)

a.imprimirContenido()