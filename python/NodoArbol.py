import pdb

class nodoArbol:
    def __init__(self):
        self.clave = None
        self.iz = None
        self.der = None
        
class arbol:
    def __init__(self):
        self.raiz = None
    
    def add(self, val):
        if(self.raiz == None):
            self.raiz = nodoArbol()
            self.raiz.clave = val
        else:
            self._add(val, self.raiz)
    
    def _add(self, val, nodo):
        
        if(val < nodo.clave):
            if(nodo.iz != None):
                self._add(val, nodo.iz)
            else:
                nodo.iz = nodoArbol()
                nodo.iz.clave = val
                
        else:
            if(nodo.der != None):
                self._add(val, nodo.der)
            else:
                nodo.der = nodoArbol()
                nodo.der.clave = val
    
    def printTree(self):
        if(self.raiz != None):
            self._printTree(self.raiz)

    def _printTree(self, node):
        if(node != None):
            self._printTree(node.iz)
            print (str(node.clave) + ' ')
            self._printTree(node.der)     
    

            
    def _sumNivel(self,nodo,nivel,nivelaux,suma):
        # pdb.set_trace()
        if(None == nodo):
            return suma
        elif (nivel == nivelaux):
            return suma + nodo.clave
        else: 
            suma = self._sumNivel(nodo.iz, nivel, nivelaux+1, suma)
            suma = self._sumNivel(nodo.der, nivel, nivelaux+1, suma)
            return suma
    
    def sumNivel(self, nivel):
        if (self.raiz != None):
            return self._sumNivel(self.raiz, nivel, 0, 0)
        else:
            return 0            
            
    def sumaNivelesIguales(self, nivel1, nivel2):
        return self.sumNivel(nivel1) == self.sumNivel(nivel2)
        
    
# arbol1 = arbol()
# arbol1.add(10)
# arbol1.add(7)
# arbol1.add(9)
# arbol1.add(2)
# arbol1.add(20)
# arbol1.add(15)
# arbol1.add(30)

# arbol1.printTree()
# print("La suma del nivel 1 es :" + str (arbol1.sumNivel(1)))
# print("La suma del nivel 2 es :" + str (arbol1.sumNivel(2)))
# print (arbol1.sumaNivelesIguales(0,1))


arbol1 = arbol()
arbol1.add(10)
arbol1.add(5)
arbol1.add(4)
arbol1.add(20)
arbol1.add(21)


arbol1.printTree()
print("La suma del nivel 1 es :" + str (arbol1.sumNivel(1)))
print("La suma del nivel 2 es :" + str (arbol1.sumNivel(2)))
print (arbol1.sumaNivelesIguales(1,2))