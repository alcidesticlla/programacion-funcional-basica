import pdb

class cola:
    def __init__(self):
        self.elementos = []
        
    def encolar(self,elemento):
        self.elementos.append(elemento)
        
    def desencolar(self): 
        if (not self.colaVacia()):
            aux = self.elementos[0]
            self.elementos = self.elementos[1:]
            return aux
        else:
            print("La cola esta vacia {desencolar}")
            
    def colaVacia(self):
        return len(self.elementos) == 0
    
    def leerCola(self, cantidad):
        for elem in range(cantidad):
            aux = input("Ingrese nuevo elemento: ")
            self.encolar(aux)
            
    def imprimirCola(self):
        print(self.elementos)
        
    def invertirCola(self):
        return [] # falta implementar
    
    def numElemCola(self):
        return len(self.elementos)
        
    def primero(self):
        if (not self.colaVacia()):
            return self.elementos[0]
        else:
            print("La cola esta vacia {primero}")
            return None
    
    def quitarPrimero(self):
        if (not self.colaVacia()):
            self.elementos = self.elementos[1:]
        else:
            print("La cola esta vacia {desencolar}") 
    
    def eliminarCola(self, cola):
        #pdb.set_trace()
        while(not cola.colaVacia()):
            cola.quitarPrimero();
            
cola1 = cola()
cola1.encolar(3)
cola1.encolar(2)
cola1.encolar(1)
cola1.encolar(0)
cola1.imprimirCola()

cola2 = cola()
cola2.encolar(3)
cola2.encolar(2)
cola2.encolar(1)
cola2.encolar(0)
cola2.imprimirCola()
cola2.eliminarCola(cola2)
cola2.imprimirCola()

         