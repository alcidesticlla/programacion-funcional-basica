from jugador import Jugador
import pdb


class Roaster:
    def __init__(self):
        self.jugadores = []

    def __str__(self):
        # pdb.set_trace()
        result = ""
        for jugador in self.jugadores:
            result = result + str(jugador) + "\n"

        return result

    # Gestionar los jugador,
    def insertar_jugador(self, jugador):
        self.jugadores.append(jugador)

    # Gestionar los jugador,
    def elimmar_jugador(self, jugador):
        return True # retorna true si el equipo  fue eliminado

    def buscar_jugador(self, nombre):
        jugador = None
        for jug in self.jugadores:
            if (jug.obtener_nombre == nombre):
                jugador = jug
                break
        return jugador
# e1 = Roaster()
# j1 = Jugador("Iden","Ticlla")
# j2 = Jugador("Alex","Ticlla")
# e1.insertar_jugador(j1)
# e1.insertar_jugador(j2)
# print (e1)
