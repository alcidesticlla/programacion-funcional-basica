
# Esta es la applicacion principal para el proyecto beisbol
#
import pdb
from serie import Serie
from equipo import Equipo

def presstocontinue():
    a = input("\nPreciona Enter para continuar . . .")

def app_name():
    print("==================================================")
    print("=", end = " ")
    print("       beisbol Application", end = " ")
    print("                    =")
    print("==================================================")

def main():
    serie = Serie()
    serie.insertar_equipo(Equipo("TECNOLOGIA"))
    serie.insertar_equipo(Equipo("FICTT"))
    serie.insertar_equipo(Equipo("VETERINARIA"))
    serie.insertar_equipo(Equipo("MEDICINA"))
    serie.insertar_equipo(Equipo("HUMANIDADES"))
    serie.insertar_equipo(Equipo("FARMACIA"))

    while(True):
        app_name()
        print("1: Gestionar los equipos.")
        print("2: Gestionar jugadores por equipo.")
        print("3: Intercambiar jugadores entre equipo.")
        print("4: Mostrar estadísticas por equipo.")
        print("5: Mostrar estadísticas por jugadores.")
        print("6: Mostrar estadísticas generales de la serie.")
        print("7: Salir")
        opcion = int(input("Ingrese una Opcion: "))

        if (opcion == 1):
            while(True):
                print("")
                print("=======================================  GESTIONAR EQUIPO  =======================================================")
                print("1: Listar equipos")
                print("2: Agregar Equipos")
                print("3: Eliminar Equipo")
                print("4: Salir")
                optequipo = int(input("\nIngrese una Opcion: "))

                if(optequipo == 1):
                    serie.listar_equipos()
                    presstocontinue()
                    break

                if(optequipo == 2):
                    nombre = input("Ingrese Nombre del equipo:  ")
                    equipo_aux = Equipo(nombre)
                    serie.insertar_equipo(equipo_aux)
                    serie.listar_equipos()
                    presstocontinue()
                    break
                if(optequipo == 3):
                    nombre = input("NOmbre del equipo a eliminar: ")
                    serie.eliminar_equipo(nombre)
                    serie.listar_equipos()
                    presstocontinue()
                    break


                if(optequipo == 4):
                    break

        if (opcion == 2):

                while(True):
                    print("1: Agregar Jugador por equipo")
                    print("2: Listar Jugadores por equipo")
                    print("3: Listar equipos con sus respectivos Jugadores")
                    print("4: salir")
                    opt = int(input("Elija una opcion: "))
                    # pdb.set_trace()
                    if (opt == 1):
                        nombre = input("Ingresar Nombre de Equipo: ")
                        serie.agregar_jugador_equipo(nombre)

                    if (opt == 2):
                        nombre = input("Ingresar Nombre de Equipo: ")
                        serie.listar_jugador_equipo(nombre)
                    if (opt == 3):
                        serie.listar_equipos(True)
                    if (opt == 4):
                        break
        if (opcion == 3):
            while(True):
                print("1: Intercambiar Jugador")
                print("2: Salir")
                opt = int(input("\nElige una opcion"))

                if(opt == 1):
                    equipo1 = input("Nombre del equipo a hacer cambio: ")
                    jugador1 = input("Nombre del jugador a hacer cambio: ")
                    equipo2 = input("Nombre del equipo 2 a hacer cambio: ")
                    jugador2 = input("Nombre del eJugador 2 a hacer cambio: ")
                    serie.intercambiar_jugador(equipo1, jugador1, equipo2, jugador2)

                if (opt == 2): break
        if (opcion == 7):
            exit()
            break

if  __name__ =='__main__':main()
