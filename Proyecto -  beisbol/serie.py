from equipo import Equipo
from jugador import Jugador
class Serie:
    def __init__(self):
        self.equipos = []

    def insertar_equipo(self,equipo):
        self.equipos.append(equipo)

    def eliminar_equipo(self, nombre):
        index = -1
        for i in range (len(self.equipos)):
            if(self.equipos[i].nombre == nombre):
                index = i
                break
        if (index != -1):
            print("Eliminando equipo : " + self.equipos[index].obtener_nombre())
            self.equipos = self.equipos[:index] + self.equipos[index + 1:]
        else:
            print("El equipo " + nombre + " No existe!!!")

    def listar_equipos(self, jugadores = False):
        if len (self.equipos) == 0:
            print("NO hay equipos en la serie")
        else:
            print("****************************  EQUIPOS  ***********************")
            for equipo in self.equipos:
                if(jugadores == False):
                    print("* " + equipo.obtener_nombre() + " \n")
                else:
                    print("* " + str(equipo) + " \n")

    def agregar_jugador_equipo(self, nombre_equipo):
        equipo = None
        for equip in self.equipos:
            if (equip.obtener_nombre() == nombre_equipo):
                equipo = equip
                break

        if (equipo == None):
            print ("El equipo " + nombre_equipo + " no existe")
        else:
            nombre = input("Nombre Jugador: ")
            apellido = input("Apellido Jugador: ")
            jugador = Jugador(nombre, apellido)
            equipo.obtener_roaster().insertar_jugador(jugador)

    def listar_jugador_equipo(self, nombre):
        equipo = None
        for equip in self.equipos:
            if (equip.obtener_nombre() == nombre):
                equipo = equip
                break

        if (equipo == None):
            print ("El equipo " + nombre + " no existe")
        else: print(str(equipo))

    def buscar_equipo(self, nombre):
        equipo = None
        for equi in self.equipos:
            if (nombre == equi.obtener_nombre()):
                equipo = equi
                break
        return equipo

    def intercambiar_jugador(self, equipo1, jugador1, equipo2, jugador2):
        equipo_cambio1 = self.buscar_equipo(equipo1)
        if (equipo_cambio1 == None):
            print(equipo1 + "No Existe")
            return
        equipo_cambio2 = self.buscar_equipo(equipo2)
        if (equipo_cambio2 == None):
            print(equipo2 + "No Existe")
            return

        jugador1 = equipo_cambio1.obtener_roaster().buscar_jugador(jugador1)
        if(jugador1 == None):
            print(jugador1 + "No Existe en " + equipo1)

        jugador2 = equipo_cambio2.obtener_roaster().buscar_jugador(jugador2)
        if (jugador2 == None):
            print(jugador2 + "No Existe en " + equipo2)

        print("Se hizo el cambio de jugadores")
# s1 = Serie()
# s1.insertar_equipo(Equipo("TECNOLOGIA"))
# s1.insertar_equipo(Equipo("FICTT"))
# s1.insertar_equipo(Equipo("VETERINARIA"))
# s1.insertar_equipo(Equipo("MEDICINA"))
# s1.insertar_equipo(Equipo("HUMANIDADES"))
# s1.insertar_equipo(Equipo("FARMACIA"))

# s1.listar_equipos()
# s1.eliminar_equipo("FARMACIA")
# s1.listar_equipos()