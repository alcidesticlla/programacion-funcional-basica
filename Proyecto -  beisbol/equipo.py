from roaster import Roaster
from jugador import Jugador

class Equipo:
    def __init__(self, nombre):
        self.nombre =  nombre
        self.jugadores = []
        self.roaster = None

    def __str__(self):
        roaster = ''
        if self.roaster == None:
            roaster = 'No hay jugadores en '+ self.nombre
        else:
            roaster +=  str(self.roaster)
        repre =  "========================================  "+self.nombre + "  ========================================\n" + roaster
        return repre

    def set_roaster(self, roaster):
        self.roaster = roaster;

    def obtener_nombre(self):
        return self.nombre

    def buscar_jugador(self,nombre):
        return self.roaster.buscar_jugador(nombre)

    def obtener_roaster(self):
        if(self.roaster == None): self.roaster = Roaster()
        return self.roaster



# r1 = Roaster()
# j1 = Jugador("Iden","Ticlla")
# j2 = Jugador("Alex","Ticlla")
# r1.insertar_jugador(j1)
# r1.insertar_jugador(j2)
# e1 = Equipo("E.G Xammp")
# e1. set_roaster(r1)
# print(e1)